# Loosely Coupled By Python
基于numpy实现的GNSS/INS松组合代码，只支持位置观测，
不支持NHC和ZUPT和里程计
## 性能测试
### imu1:

|指标|参数|
| ---: | ---: |
|arw|0.003 deg/sqrt{h}|
|vrw|0.03 m/s/sqrt{h}|
|gyro bias stability|0.017 deg/h|
|acce bias stability|15 mGal|
GNSS中断100s性能:

|                                  | x error /m   | y error/m   | z error/m   |
| -------------------------------- | ------------ | ----------- | ----------- |
| position error(m) 1-$\sigma$     | 0.0140       | 0.0122      | 0.0080      |
| position error(m) 2-$\sigma$     | 0.0630       | 0.0629      | 0.0173      |
| position error(m) rms            | 0.0254       | 0.0232      | 0.0155      |
| velocity error(m/s) 1-$\sigma$   | 0.0027       | 0.0026      | 0.0015      |
| velocity error(m/s) 2-$\sigma$   | 0.0068       | 0.0065      | 0.0029      |
| velocity error(m/s) rms          | 0.0031       | 0.0030      | 0.0031      |
| attitude error(deg) 1-$\sigma$   | 0.0014       | 0.0016      | 0.0132      |
| attitude error(deg) 2-$\sigma$   | 0.0030       | 0.0034      | 0.0195      |
| attitude error(deg) rms          | 0.0018       | 0.0017      | 0.0112      |
| -------------------------------- | ------------ | ----------- | ----------- |
### imu2:
|指标|参数|
| ---: | ---: |
|arw|0.6 deg/sqrt{h}|
|vrw|0.5 m/s/sqrt{h}|
|gyro bias stability|5 deg/h|
|acce bias stability|180 mGal|

## 数据格式
1. imu:    

| [0]     | [1]     | [2]     | [3]     | [4]     | [5]     | [6]     |
|:--------|:--------|:--------|:--------|:--------|:--------|:--------|
| time[s] | gx(rad) | gy(rad) | gz(rad) | ax(m/s) | ay(m/s) | az(m/s) |       

2. GNSS:   
   GNSS_TXT_POS_7:
  
| [0]     | [1]     | [2]       | [3]  | [4]     | [5]     | [6]     | [7~12]  |
|:---     |:---      |:---      |:---  |:---     |:---     |:---     |:---   |
| time[s] | lat[deg] | lon[deg] | h[m] | vn[m/s] | ve[m/s] | vg[m/s] | std[m]|

   GNSS_TXT_POS_14:

| [0]     | [1]     | [2]       | [3]  | [4~6]     | [7~9]  |[10~13]
|:---     |:---      |:---      |:---  |:---       |:---   |:---|
| time[s] | lat[deg] | lon[deg] | h[m] | -111(invalid) | std(m) | -111(invalid)|