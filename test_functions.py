# -*- coding:utf-8 -*-
# @Time : 2020/9/15 下午10:04 
# @Author : rebeater
# @File : test_functions.py 
# @Project: LooselyCouple_2020
# @Function: TODO

import ins_core as ins

import numpy as np


def test():
    nav1 = ins.NavData(np.array([
        0, 200560.99800, 30.4673503122949, 114.4713695870413, 75.9233, 61.9361, 6.3545, 0.5005, - 13.666,
        1.406, 86.592
    ]))
    nav2 = ins.NavData(np.array([
        0.000000000000000, 200560.997000000003027, 30.456867223600000, 114.472536426000005,
        25.582380000000001, - 0.594990000000000, 12.581149999999999, - 0.043850000000000, - 0.490710000000000,
        0.919000000000000, 92.107140000000001
    ]))
    dn, de, dd, d = ins.get_distance(nav1, nav2)
    print("dn = %f, de = %f dd = %f d = %f" % (
        dn, de, dd, d
    ))


if __name__ == '__main__':
    test()
