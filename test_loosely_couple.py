# -*- coding: utf-8 -*-
# @Time : 2020/9/10 下午9:50
# @Author : rebeater
# @File : test.py
# @Project: LooselyCouple_2020
# @Function: Loosely Coupled Algorithm Core Functions


import loosely_couple_core as lc

if __name__ == '__main__':
    opt = lc.Option("./configure_m39.yml")
    lc.outage_evalution(opt, time_outage=50)
    # lc.post_loosely_couple(opt)
